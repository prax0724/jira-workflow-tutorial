package AddWatcher;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.plugin.workflow.WorkflowPluginFunctionFactory;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.opensymphony.workflow.loader.*;

import webwork.action.ActionContext;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * This is the factory class responsible for dealing with the UI for the post-function.
 * This is typically where you put default values into the velocity context and where you store user input.
 */

@Named("AddWatcherPostFunctionFactory")
public class AddWatcherPostFunctionFactory extends AbstractWorkflowPluginFactory implements WorkflowPluginFunctionFactory
{
    public static final String FIELD_MESSAGE = "messageField";
    
    public static final String USERS = "users";
    
    public static final String USER = "user";

    @ComponentImport
    private WorkflowManager workflowManager;

    @Inject
    public AddWatcherPostFunctionFactory(WorkflowManager workflowManager) {
        this.workflowManager = workflowManager;
    }

    @Override
    protected void getVelocityParamsForInput(Map<String, Object> velocityParams) {
     //   Map<String, String[]> myParams = ActionContext.getParameters();
     //   final JiraWorkflow jiraWorkflow = workflowManager.getWorkflow(myParams.get("workflowName")[0]);

        //the default message
       // velocityParams.put(FIELD_MESSAGE, "Workflow Last Edited By " + jiraWorkflow.getUpdateAuthorName());
        
        velocityParams.put(USERS, ComponentAccessor.getUserManager().getAllApplicationUsers() );

    }

    @Override
    protected void getVelocityParamsForEdit(Map<String, Object> velocityParams, AbstractDescriptor descriptor) {
     //   getVelocityParamsForInput(velocityParams);
    //    getVelocityParamsForView(velocityParams, descriptor);
    	
    	velocityParams.put(USERS, ComponentAccessor.getUserManager().getAllApplicationUsers() );
    }

    @Override
    protected void getVelocityParamsForView(Map<String, Object> velocityParams, AbstractDescriptor descriptor) {
        if (!(descriptor instanceof FunctionDescriptor)) {
            throw new IllegalArgumentException("Descriptor must be a FunctionDescriptor.");
        }

        FunctionDescriptor functionDescriptor = (FunctionDescriptor)descriptor;
        
       // String message = (String)functionDescriptor.getArgs().get(FIELD_MESSAGE);
        
        String user = (String)functionDescriptor.getArgs().get(USER);

        if (user == null) {
            user = "No User";
        }

        velocityParams.put(USER,user);
    }


    public Map<String,?> getDescriptorParams(Map<String, Object> formParams) {
        Map params = new HashMap();

        // Process The map
        String message = extractSingleParam(formParams,USER);
        params.put(USER,message);

        return params;
    }

}