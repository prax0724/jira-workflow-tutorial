package AddWatcher;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.ModifiedValue;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutStorageException;
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder;
import com.atlassian.jira.issue.watchers.WatcherManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.roles.ProjectRoleActors;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.workflow.function.issue.AbstractJiraFunctionProvider;
import com.opensymphony.module.propertyset.PropertySet;
//import com.opensymphony.user.User;
import com.opensymphony.workflow.WorkflowException;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

@Named("AddWatcherFunction")
public class AddWatcherPostFunction extends AbstractJiraFunctionProvider {

	@ComponentImport
	private final CustomFieldManager customFieldManager;
	@ComponentImport
	private final JiraAuthenticationContext authContext;
	@ComponentImport
	private final UserUtil userUtil;
	@ComponentImport
	private final ProjectRoleManager projectRoleManager;

	@Inject
	public AddWatcherPostFunction(CustomFieldManager customFieldManager,
			JiraAuthenticationContext authContext, UserUtil userUtil,
			ProjectRoleManager projectRoleManager) {
		this.customFieldManager = customFieldManager;
		this.authContext = authContext;
		this.userUtil = userUtil;
		this.projectRoleManager = projectRoleManager;
	}

	public void execute(Map transientVars, Map args, PropertySet ps)
			throws WorkflowException {
		addWatcher(getIssue(transientVars),  ComponentAccessor.getUserManager().getUser(args.get("user").toString()));
	}


	private void addWatcher(MutableIssue issue, ApplicationUser user) {
		
		
		WatcherManager watcherManager = ComponentAccessor.getWatcherManager();
		watcherManager.startWatching(user, issue);
	}

}
